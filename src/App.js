import React, { Component } from 'react';
import './App.css';
import TaskItem from './TaskItem';
import AddItem from './AddItem';

class App extends Component {
  constructor(props){
    super(props);

    this.state = {
      projectList: [],
      currentProjectId: 0,
      tasks: JSON.parse(localStorage.getItem('tasks'))
    };

    //thisを明示的にオーバライドすることで、deleteTask内でthisがAppインスタンスを意味するようにする
    this.deleteTask = this.deleteTask.bind(this);
    this.addTask = this.addTask.bind(this);
    this.editTask = this.editTask.bind(this);
    this.fetchTasks = this.fetchTasks.bind(this);
  }

  //
  componentDidMount(){
    this.fetchTasks();
  }

  fetchTasks(){
    fetch("http://localhost:3001/db")
    .then(response => response.json())
    .then(json=>{
      this.setState({
        projectList: Object.keys(json),
      },()=>{
        this.setState({
          tasks: json[this.state.projectList[this.state.currentProjectId]]
        })
      })
    })
  }

  getTask(){
    return this.state.tasks;
  }

  deleteTask(id){
    console.log(id);
    fetch(`http://localhost:3001/${this.state.projectList[this.state.currentProjectId]}/${id}`,{
      method: "DELETE"
    })
    .then(this.fetchTasks);
  }

  addTask(name,description){
    fetch(`http://localhost:3001/${this.state.projectList[this.state.currentProjectId]}`,{
      method: "POST",
      headers:{
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({"name":name, "description": description})
    })
    .then(this.fetchTasks);
  }

  editTask(name,description,id){
    fetch(`http://localhost:3001/${this.state.projectList[this.state.currentProjectId]}/${id}`,{
      method: "PUT",
      headers:{
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({"name":name, "description": description})
    })
    .then(this.fetchTasks);
  }

  render(){
    return (
      <div className="App">
        <h1>TODAY'S TASKS</h1>
        <ul className="taskList">
        {
          this.state.tasks.map((task, index) => {
            return (
              <TaskItem 
                key = {index}
                {...task}
                deleteTask = {this.deleteTask}
                editTask = {this.editTask}
              />
            );
          })
        }
        </ul>
        <AddItem 
          addTask = {this.addTask}
        />
      </div>
    );
  }
}

export default App;
