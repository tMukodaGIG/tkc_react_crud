import React, {Component} from 'react';

class TaskItem extends Component{
    constructor(props){
        super(props);
        this.deleteTask = this.deleteTask.bind(this);
        this.editTask = this.editTask.bind(this);
        this.onEdit = this.onEdit.bind(this);
        this.handleChange = this.handleChange.bind(this);

        this.state = {
            isEdit: false,
            name: '',
            description: ''
        };
    }

    handleChange(event){
        this.setState({[event.target.name]: event.target.value});
    }

    onEdit(){
        console.log('edit mode');
        this.setState({
            isEdit: true,
            name: this.props.name,
            description: this.props.description
        });
    }

    editTask(event){
        event.preventDefault();
        this.props.editTask(this.state.name, this.state.description, this.props.id);
        this.setState({
            isEdit: false
        });
    }

    deleteTask(){
        const {id, deleteTask} = this.props;
        deleteTask(id);
    }

    render(){
        const {name,description} = this.props; //オブジェクトの分割代入
        return(
            this.state.isEdit ? (
                <form onSubmit={this.editTask} >
                <input placeholder="タスク名" name = "name" defaultValue={name} onChange={this.handleChange}/>
                <input placeholder="タスク内容" name = "description" defaultValue={description} onChange={this.handleChange}/>
                <button>保存</button>
                </form>
            ):(
                <li className="taskList__item">
                <span className="taskList__itemName">{name}</span>
                <span className="taskList__itemDescription">{description}</span>
                <div>
                <button className="button isEdit" onClick={this.onEdit}>編集</button>
                <button className="button isDelete" onClick={this.deleteTask}>片した！</button>
                </div>
          </li>
            )
        )
    }
}

export default TaskItem;