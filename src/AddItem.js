// import React, {Component} from 'react';

import React, {useState,useCallback} from 'react';

/***関数を使った時のバージョン***/
function AddTask(props){
    //declare state variables
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');

    const onSubmit = useCallback((e) =>{
        e.preventDefault();
        props.addTask(name,description);
    },[name, description]);

    return(
        <form onSubmit={onSubmit}>
            <input placeholder="タスク名" name="name" onChange={(e)=>{setName(e.target.value)}}/>
            <input placeholder="タスク内容" name="description" onChange={(e)=>setDescription(e.target.value)}/>
            <button>追加</button>
        </form>
    )
}

/***クラスを使った時のバージョン***/

// class TaskItem extends Component{
//     constructor(props){
//         super(props);
//         this.state = {
//             name: '',
//             description: '',
//         };

//         //Controlled Component: inputに入力された値を管理する
//         this.onSubmit = this.onSubmit.bind(this);
//         this.handleChange = this.handleChange.bind(this);
//     }

//     //inputに入力された値をstateに反映
//     handleChange(event){
//         this.setState({[event.target.name]: event.target.value});
//         console.log(event.target.value);
//     }

//     //リフティング
//     onSubmit(event){
//         event.preventDefault();
//         this.props.addTask(this.state.name,this.state.description);
//     }

//     render(){
//         return(
//             <form onSubmit={this.onSubmit}>
//                 <input placeholder="タスク名" name="name" onChange={this.handleChange}/>
//                 <input placeholder="タスク内容" name="description" onChange={this.handleChange}/>
//                 <button>追加</button>
//             </form>
//         )
//     }
// }

export default AddTask;